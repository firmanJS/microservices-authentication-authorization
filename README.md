# micro-service-authentication-authorization-sample

### import postman
- micro-app.postman_collection.json

### Install Dependencies and Usage
- To start up  **micro-service-authentication-authorization-sample**
```sh
docker-compose up --build
```
