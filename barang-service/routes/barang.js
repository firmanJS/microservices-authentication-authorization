var express = require('express');
const Barang = require('../controllers/BarangController');
const check = require('../helpers/TokenValidaton')
const bar = new Barang()
var router = express.Router();
/*
  check.verifyToken function for check token in helper/TokenValidation
  check.verifyAuthorizationView function for check access view role in helper/TokenValidation
  check.verifyAuthorizationEdit function for check access edit role in in helper/TokenValidation
  check.verifyAuthorizationSave function for check access save role in in helper/TokenValidation
  check.verifyAuthorizationDelete function for check access delete role in in helper/TokenValidation
  check.RedisAutoFlush function for delete older redis key set new redis key
*/
router.get('/barang',check.verifyToken,check.verifyAuthorizationView,bar.Show);
router.get('/barang/:id',check.verifyToken,check.verifyAuthorizationEdit,bar.Edit);
router.patch('/barang/:id',check.verifyToken,check.verifyAuthorizationEdit,check.RedisAutoFlush,(req,res) =>{
  bar.Update(req,res,req.params.id,JSON.parse(JSON.stringify(req.body)))
});
router.post('/barang',check.verifyToken,check.verifyAuthorizationSave,check.RedisAutoFlush,(req,res) => {
  bar.Create(res,JSON.parse(JSON.stringify(req.body)))
});
router.delete('/barang/:id',check.verifyToken,check.verifyAuthorizationDelete,check.RedisAutoFlush,(req,res) => {
  bar.Destroy(req,res,req.params.id)
});
module.exports = router;
